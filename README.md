# Webshell

Single-file python web shell.

## How to use


```
$ wget https://code.peren.gouv.fr/peren/infrastructure/webshell/-/raw/stable/shell.py
$ python3 shell.py --port=4200
listening on :4200
server probably started. Control page available under /index.html .
```

Then visit https://<server_name>:4200/index.html to get your web shell.

## History

- Source : https://github.com/Najones19746/webShell/blob/master/shell.py by Nick
- Changes by PEReN :
   - update to Python3
   - modernize JS
   - address / port selection via command line
   - socket listening
   - daemonization
